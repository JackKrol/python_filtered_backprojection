#Author: John A. Krol

import numpy as np

def rotate_image(angle_deg, image, sizex=None, sizey=None):
    #Input sizex and sizey to save on computation time
    if sizex==None or sizey==None:
        sizex = np.size(image,0)
        sizey = np.size(image,1)
    rotated = np.zeros([sizex, sizey])
    for xit in range(0, sizex - 1):
        for yit in range(0, sizey - 1):
            xpyp = _rotated_coord(angle_deg, xit, yit, sizex / 2, sizey / 2)
            rotated[xit, yit] = _bilinear_interpolation(xpyp[0], xpyp[1], image, sizex, sizey)
    return rotated

def _rotated_coord(angle_deg, x, y, xmid, ymid):
    angle_rad = np.deg2rad(angle_deg)
    xp = (x - xmid) * np.cos(angle_rad) + (y - ymid) * np.sin(angle_rad) + xmid
    yp = -(x - xmid) * np.sin(angle_rad) + (y - ymid) * np.cos(angle_rad) + ymid
    return (xp, yp)

def _bilinear_interpolation(x, y, image, sizex, sizey):
    x1 = np.clip(np.floor(x), 0, sizex-2)
    y1 = np.clip(np.floor(y), 0, sizey-2)
    x2 = x1 + 1
    y2 = y1 + 1

    fxy1 = image[int(x1), int(y1)] * (x2 - x) / (x2 - x1) \
           + image[int(x2), int(y1)] * (x - x1) / (x2 - x1)
    fxy2 = image[int(x1), int(y2)] * (x2 - x) / (x2 - x1) \
           + image[int(x2), int(y2)] * (x - x1) / (x2 - x1)

    return fxy1 * (y2 - y) / (y2 - y1) + fxy2 * (y - y1) / (y2 - y1)